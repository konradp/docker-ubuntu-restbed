# docker-ubuntu-restbed

A Docker image of Ubuntu Linux with Restbed C++ dev library and gcc/make buildtools.

Build
```
./update.sh
```
The image can be run locally.
```
./run.sh
```
