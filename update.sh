#!/bin/bash
REGISTRY=registry.gitlab.com/konradp/docker-ubuntu-restbed
docker login registry.gitlab.com
docker build -t $REGISTRY/ubuntu:restbed .
docker push $REGISTRY/ubuntu:restbed
