FROM ubuntu
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install build-essential libcurl4-gnutls-dev librestbed-dev \
  libtinyxml2-dev
